(function(window,google,mapster){
	var mycenter = new google.maps.LatLng(18.5455968,73.82974380000002);
	mapster.MAP_OPTIONS = {
	  center:mycenter, 
        	  zoom:12, 
        	       mapTypeId:google.maps.MapTypeId.ROADMAP
	};
}(window,google,window.Mapster || (window.Mapster = {})));
