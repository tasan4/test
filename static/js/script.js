(function(window,google,mapster){
 
	var mycenter = new google.maps.LatLng(18.5455968,73.82974380000002);
       	function loadMap() {
        	var mapOptions = {
       		       center:mycenter, 
        	       zoom:12, 
        	       mapTypeId:google.maps.MapTypeId.ROADMAP
         	    };
	    
            var input = document.getElementById('locationTextField');
            var autocomplete = new google.maps.places.Autocomplete(input);
	
            var map = new google.maps.Map(document.getElementById("sample"),mapOptions);
		
	    
		var maker = new google.maps.Marker({
		position:mycenter,
		animation:google.maps.Animation.BOUNCE		
		});
            
 
	    var searchBox = new google.maps.places.SearchBox(input);
            
	    // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function() {
            searchBox.setBounds(map.getBounds());
            });
	 
	    var markers = [];
	    markers.push(maker);
           // Listen for the event fired when the user selects a prediction and retrieve
           // more details for that place.
            searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();

           	if (places.length == 0) {
                  return;
                }
		// Clear out the old markers.
          	markers.forEach(function(marker) {
           	 marker.setMap(null);
		});
		
		//markers = [];
		// For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
		places.forEach(function(place) {
            	var icon = {
              		url: place.icon,
              		size: new google.maps.Size(71, 71),
              		origin: new google.maps.Point(0, 0),
              		anchor: new google.maps.Point(17, 34),
              		scaledSize: new google.maps.Size(25, 25)
            		}

		// Create a marker for each place.
                markers.push(new google.maps.Marker({
              		map: map,
              		title: place.name,
              		position: place.geometry.location,
			animation:google.maps.Animation.BOUNCE
            		}));

		if (place.geometry.viewport) {
	        // Only geocodes have viewport.
		        bounds.union(place.geometry.viewport);
	        } 
		else {
	        	bounds.extend(place.geometry.location);
      		}

		});
		
		map.fitBounds(bounds);

            });
	    
	   markers.forEach(function(marker) {
           	 marker.setMap(map);
		});
		

         };
		
	



         google.maps.event.addDomListener(window, 'load', loadMap);


}(window,google,window.Mapster || (window.Mapster = {})));
